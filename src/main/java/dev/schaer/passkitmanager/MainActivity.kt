package dev.schaer.passkitmanager

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import dev.schaer.passkitmanager.dev.schaer.passkit.model.FieldType
import dev.schaer.passkitmanager.dev.schaer.passkit.persistence.DatabaseSingleton
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val dao = DatabaseSingleton.getDb(applicationContext).passDao()
        dao.getAll().forEach {
            // TODO build nicer listing
            val text = TextView(applicationContext)
            text.text = it.fields
                .filter { field -> field.type == FieldType.headerFields }
                .joinToString { field -> field.value }
            main_list.addView(text)
            text.setOnClickListener {
                val intent = Intent(this, DetailActivity::class.java)
                // TODO open DetailView of intent on click
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}
