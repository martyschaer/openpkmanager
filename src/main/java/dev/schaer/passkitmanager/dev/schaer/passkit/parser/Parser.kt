package dev.schaer.passkitmanager.dev.schaer.passkit.parser

import android.graphics.Color
import dev.schaer.passkitmanager.dev.schaer.passkit.model.*
import org.json.JSONObject

fun parseJson(json: String, resources: List<Resource>): PassWithData {
    val passJson = JSONObject(json)
    val pass = extractPassInformation(passJson)
    val barcode = extractBarcode(passJson)
    val fields = accumulateAllFields(passJson.optJSONObject(pass.type.name))
    return PassWithData(
        pass = pass,
        barcode = barcode,
        fields = fields,
        resources = resources
    )
}

private fun accumulateAllFields(structure: JSONObject?): List<Field> {
    return FieldType.values().flatMap { accumulateFields(structure, it) }
}

private fun accumulateFields(structure: JSONObject?, type: FieldType): List<Field> {
    structure ?: return emptyList()

    val fields = listOf<Field>().toMutableList()
    structure.optJSONArray(type.name)?.run {
        for (i in 0 until length()) {
            fields.add(fieldFromJsonObject(this.optJSONObject(i), type))
        }
    }
    return fields
}

private fun extractPassInformation(passJson: JSONObject): Pass {
    val fgColor = colorFromCSSRGB(passJson.optString("foregroundColor"), Color.BLACK)
    val bgColor = colorFromCSSRGB(passJson.optString("backgroundColor"), Color.WHITE)
    val labelColor = colorFromCSSRGB(passJson.optString("labelColor"), Color.DKGRAY)
    return Pass(
        foregroundColor = fgColor,
        backgroundColor = bgColor,
        labelColor = labelColor,
        type = determinePassType(passJson)
    )
}

private fun determinePassType(passJson: JSONObject): PassType {
    val passTypes = PassType.values().map { it.name }
    val typeCandidates = passJson.keys().asSequence().toSet().intersect(passTypes)
    return typeCandidates.first()?.let { PassType.valueOf(it) } ?: PassType.generic
}

private fun extractBarcode(passJson: JSONObject): Barcode? {
    extractBarcodeJson(passJson)?.let {
        val message = it.getString("message")
        val format = Format.valueOf(it.getString("format"))
        val altText = it.optString("altText")
        return Barcode(
            message = message,
            format = format,
            altText = altText
        )
    }
    return null
}

/**
 * Older pkpass formats may not contain a barcode array, but instead a single barcode.
 */
private fun extractBarcodeJson(passJson: JSONObject): JSONObject? {
    return passJson.optJSONArray("barcodes")?.getJSONObject(0)
        ?: passJson.optJSONObject("barcode")
}

private fun colorFromCSSRGB(cssRGB: String, default: Int): Int {
    val regex = """rgb\((?<r>\d+),\s*(?<g>\d+),\s*(?<b>\d+)\)""".toRegex()
    val groups: MatchGroupCollection? = regex.matchEntire(cssRGB)?.groups
    groups?.let {
        val r: Int = it[1]?.value?.toInt() ?: return default
        val g: Int = it[2]?.value?.toInt() ?: return default
        val b: Int = it[3]?.value?.toInt() ?: return default
        return Color.rgb(r, g, b)
    }
    return default
}
