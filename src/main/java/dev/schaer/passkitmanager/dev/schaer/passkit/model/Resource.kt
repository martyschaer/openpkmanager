package dev.schaer.passkitmanager.dev.schaer.passkit.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import dev.schaer.passkitmanager.dev.schaer.passkit.persistence.ResourceTypeConverter

@TypeConverters(ResourceTypeConverter::class)
@Entity
class Resource(
    @PrimaryKey(autoGenerate = true) val uid: Long = 0,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "type") val type: ResourceType,
    @ColumnInfo(name = "data") val data: ByteArray,
    @ColumnInfo(name = "fkPassId") var fkPassId: Long? = null
)

enum class ResourceType {
    PNG,
    PKPASS,
    JSON
}

