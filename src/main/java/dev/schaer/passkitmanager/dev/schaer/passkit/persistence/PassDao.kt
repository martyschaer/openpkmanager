package dev.schaer.passkitmanager.dev.schaer.passkit.persistence

import androidx.room.*
import dev.schaer.passkitmanager.dev.schaer.passkit.model.*

@Dao
interface PassDao {
    @Transaction
    @Query("SELECT * FROM pass")
    fun getAll(): List<PassWithData>

    @Insert
    fun insert(pass: Pass): Long

    @Insert
    fun insertAll(vararg fields: Field): Array<Long>

    @Insert
    fun insertAll(vararg resources: Resource): Array<Long>

    @Insert
    fun insertAll(vararg barcodes: Barcode): Array<Long>

    @Delete
    fun delete(pass: Pass)

    @Delete
    fun delete(vararg fields: Field)

    @Delete
    fun delete(vararg resources: Resource)

    @Delete
    fun delete(vararg barcodes: Barcode)
}