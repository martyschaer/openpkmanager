package dev.schaer.passkitmanager.dev.schaer.passkit.model

import android.graphics.Bitmap
import androidx.room.*
import dev.schaer.passkitmanager.dev.schaer.passkit.barcode.generate
import dev.schaer.passkitmanager.dev.schaer.passkit.persistence.BarcodeFormatConverter
import org.json.JSONObject
import java.nio.charset.Charset

@TypeConverters(BarcodeFormatConverter::class)
@Entity
data class Barcode(
    @PrimaryKey(autoGenerate = true) val uid: Long = 0,
    @ColumnInfo(name = "format") val format: Format,
    @ColumnInfo(name = "message") val message: String,
    // TODO enable multiple charsets @ColumnInfo(name = "encoding") val encoding: Charset = Charsets.ISO_8859_1,
    @ColumnInfo(name = "alt_text") val altText: String,
    @ColumnInfo(name = "fkPassId") var fkPassId: Long? = null
) {
    fun bitmap(dimensions: Int = 500): Bitmap {
        return generate(message, dimensions, format)
    }
}

enum class Format {
    PKBarcodeFormatQR,
    PKBarcodeFormatPDF417,
    PKBarcodeFormatAztec,
    PKBarcodeFormatCode128
}