package dev.schaer.passkitmanager.dev.schaer.passkit.persistence

import androidx.room.TypeConverter
import dev.schaer.passkitmanager.dev.schaer.passkit.model.*

class BarcodeFormatConverter {
    @TypeConverter
    fun fromFormat(format: Format): Int {
        return format.ordinal
    }

    @TypeConverter
    fun toFormat(ordinal: Int): Format {
        return Format.values()[ordinal]
    }
}

class PassTypeConverter {
    @TypeConverter
    fun fromPassType(passType: PassType): Int {
        return passType.ordinal
    }

    @TypeConverter
    fun toPassType(ordinal: Int): PassType {
        return PassType.values()[ordinal]
    }
}

class FieldAlignmentConverter {
    @TypeConverter
    fun fromFieldAlignment(alignment: Alignment): Int {
        return alignment.ordinal
    }

    @TypeConverter
    fun toFieldAlignment(ordinal: Int): Alignment {
        return Alignment.values()[ordinal]
    }
}

class FieldTypeConverter {
    @TypeConverter
    fun fromFieldType(fieldType: FieldType): Int {
        return fieldType.ordinal
    }

    @TypeConverter
    fun toFieldType(ordinal: Int): FieldType {
        return FieldType.values()[ordinal]
    }
}

class ResourceTypeConverter {
    @TypeConverter
    fun fromResourceType(resourceType: ResourceType): Int {
        return resourceType.ordinal
    }

    @TypeConverter
    fun toResourceType(ordinal: Int): ResourceType {
        return ResourceType.values()[ordinal]
    }
}
