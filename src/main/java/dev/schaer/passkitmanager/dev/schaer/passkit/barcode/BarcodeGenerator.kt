package dev.schaer.passkitmanager.dev.schaer.passkit.barcode

import android.graphics.Bitmap
import android.graphics.Color
import androidx.core.graphics.set
import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType
import com.google.zxing.Writer
import com.google.zxing.aztec.AztecWriter
import com.google.zxing.common.BitMatrix
import com.google.zxing.oned.Code128Writer
import com.google.zxing.pdf417.PDF417Writer
import com.google.zxing.qrcode.QRCodeWriter
import dev.schaer.passkitmanager.dev.schaer.passkit.model.Format

private fun BitMatrix.toBitMap(): Bitmap {
    val bm: Bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
    for (x in 0 until width) {
        for (y in 0 until height) {
            bm[x, y] = if (this[x, y]) Color.BLACK else Color.WHITE
        }
    }

    return bm
}

fun generate(value: String, dimensions: Int, format: Format): Bitmap {
    return writer(format).encode(
        value,
        barcodeFormat(format),
        dimensions,
        dimensions,
        hints(format)
    ).toBitMap()
}

private fun hints(format: Format): Map<EncodeHintType, *> {
    return when (format) {
        Format.PKBarcodeFormatQR -> mapOf(EncodeHintType.MARGIN to 2)
        else -> emptyMap<EncodeHintType, Unit>()
    }
}

private fun barcodeFormat(format: Format): BarcodeFormat {
    return when (format) {
        Format.PKBarcodeFormatQR -> BarcodeFormat.QR_CODE
        Format.PKBarcodeFormatPDF417 -> BarcodeFormat.PDF_417
        Format.PKBarcodeFormatAztec -> BarcodeFormat.AZTEC
        Format.PKBarcodeFormatCode128 -> BarcodeFormat.CODE_128
    }
}

private fun writer(format: Format): Writer {
    return when (format) {
        Format.PKBarcodeFormatQR -> QRCodeWriter()
        Format.PKBarcodeFormatPDF417 -> PDF417Writer()
        Format.PKBarcodeFormatAztec -> AztecWriter()
        Format.PKBarcodeFormatCode128 -> Code128Writer()
    }
}

