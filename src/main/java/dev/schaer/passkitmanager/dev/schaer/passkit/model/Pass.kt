package dev.schaer.passkitmanager.dev.schaer.passkit.model

import android.graphics.Color
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import dev.schaer.passkitmanager.dev.schaer.passkit.persistence.PassTypeConverter

@TypeConverters(PassTypeConverter::class)
@Entity
class Pass(
    @PrimaryKey(autoGenerate = true) val passId: Long = 0,
    @ColumnInfo(name = "foreground_color") val foregroundColor: Int = Color.BLACK,
    @ColumnInfo(name = "background_color") val backgroundColor: Int = Color.WHITE,
    @ColumnInfo(name = "label_color") val labelColor: Int = Color.DKGRAY,
    @ColumnInfo(name = "pass_type") val type: PassType
)

enum class PassType {
    boardingPass,
    coupon,
    eventTicket,
    generic,
    storeCard
}
