package dev.schaer.passkitmanager.dev.schaer.passkit.model

import android.content.Context
import android.graphics.Color
import android.view.ViewGroup
import android.widget.TableLayout
import android.widget.TableRow
import android.widget.TextView
import androidx.core.view.setMargins
import androidx.room.*
import dev.schaer.passkitmanager.dev.schaer.passkit.persistence.FieldAlignmentConverter
import dev.schaer.passkitmanager.dev.schaer.passkit.persistence.FieldTypeConverter
import org.json.JSONObject

@TypeConverters(FieldTypeConverter::class, FieldAlignmentConverter::class)
@Entity
data class Field(
    @PrimaryKey val key: String,
    @ColumnInfo(name = "value") val value: String,
    @ColumnInfo(name = "label") val label: String = "",
    @ColumnInfo(name = "type") val type: FieldType,
    @ColumnInfo(name = "alignment") val alignment: Alignment = Alignment.PKTextAlignmentNatural,
    @ColumnInfo(name = "fkPassId") var fkPassId: Long? = null
) {
    fun fieldView(
        context: Context,
        labelColor: Int = Color.DKGRAY,
        valueColor: Int = Color.BLACK
    ): TableLayout {
        val table = TableLayout(context)
        table.addView(textRow(label, context, labelColor))
        table.addView(textRow(value, context, valueColor))
        val params = ViewGroup.MarginLayoutParams(
            ViewGroup.MarginLayoutParams.WRAP_CONTENT,
            ViewGroup.MarginLayoutParams.WRAP_CONTENT
        )
        params.setMargins(10)
        table.layoutParams = params
        return table
    }

    private fun textRow(
        text: String,
        context: Context,
        textColor: Int
    ): TableRow {
        val row = TableRow(context)
        val textView = TextView(context)
        textView.text = text
        textView.setTextColor(textColor)
        row.addView(textView)
        return row
    }

    fun fontSize(): Float {
        return when (type) {
            FieldType.primaryFields -> 20f
            FieldType.secondaryFields -> 15f
            FieldType.auxiliaryFields -> 12f
            else -> 11f
        }
    }
}

fun fieldFromJsonObject(json: JSONObject, type: FieldType): Field {
    val alignmentStr = json.optString("textAlignment", Alignment.PKTextAlignmentNatural.name)
    val alignment = Alignment.valueOf(alignmentStr)
    val key = json.optString("key")
    val label = json.optString("label")
    val value = json.optString("value", "ŋułł")
    return Field(key = key, label = label, value = value, type = type, alignment = alignment)
}

enum class Alignment {
    PKTextAlignmentLeft,
    PKTextAlignmentCenter,
    PKTextAlignmentRight,
    PKTextAlignmentNatural
}

enum class FieldType {
    auxiliaryFields,
    backFields,
    headerFields,
    primaryFields,
    secondaryFields,
    transitType // required for boarding passes, otherwise not allowed
}