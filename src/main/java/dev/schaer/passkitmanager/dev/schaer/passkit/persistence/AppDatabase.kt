package dev.schaer.passkitmanager.dev.schaer.passkit.persistence

import androidx.room.Database
import androidx.room.RoomDatabase
import dev.schaer.passkitmanager.dev.schaer.passkit.model.Barcode
import dev.schaer.passkitmanager.dev.schaer.passkit.model.Field
import dev.schaer.passkitmanager.dev.schaer.passkit.model.Pass
import dev.schaer.passkitmanager.dev.schaer.passkit.model.Resource

@Database(
    entities = [Pass::class, Field::class, Barcode::class, Resource::class],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun passDao(): PassDao
}