package dev.schaer.passkitmanager.dev.schaer.passkit.persistence

import android.content.Context
import androidx.room.Room

object DatabaseSingleton {
    private var dbInstance: AppDatabase? = null
    fun getDb(context: Context): AppDatabase {
        return dbInstance ?: Room.databaseBuilder(
            context,
            AppDatabase::class.java, "pass-db"
        ).allowMainThreadQueries().build()
    }
}
