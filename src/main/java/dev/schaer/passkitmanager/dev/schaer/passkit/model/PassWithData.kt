package dev.schaer.passkitmanager.dev.schaer.passkit.model

import androidx.room.Embedded
import androidx.room.Relation

data class PassWithData(
    @Embedded val pass: Pass,
    // TODO support multiple barcodes
    @Relation(parentColumn = "passId", entityColumn = "fkPassId") val barcode: Barcode?,
    @Relation(parentColumn = "passId", entityColumn = "fkPassId") val fields: List<Field>,
    @Relation(parentColumn = "passId", entityColumn = "fkPassId") val resources: List<Resource>
)