package dev.schaer.passkitmanager

import android.content.ContentResolver
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.util.TypedValue
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.TableLayout
import android.widget.TableRow
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import dev.schaer.passkitmanager.dev.schaer.passkit.model.*
import dev.schaer.passkitmanager.dev.schaer.passkit.parser.parseJson
import dev.schaer.passkitmanager.dev.schaer.passkit.persistence.DatabaseSingleton
import kotlinx.android.synthetic.main.activity_detail.*
import org.json.JSONObject
import java.io.FileInputStream
import java.io.InputStream
import java.util.zip.ZipInputStream

class DetailActivity : AppCompatActivity() {

    private var passWithData: PassWithData? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        setSupportActionBar(toolbar_detail)

        handleIntent()
    }

    private fun handleIntent() {
        val uri: Uri? = intent?.data
        val attachment: InputStream? = when (uri?.scheme) {
            ContentResolver.SCHEME_CONTENT -> contentResolver.openInputStream(uri)
            else -> uri?.encodedPath?.let { FileInputStream(it) }
        }
        attachment?.let { handleZip(it) }
    }

    private fun handleZip(input: InputStream) {
        val zipContent = readZip(input)
        val resources = mapZipContentToResources(zipContent)

        zipContent["pass.json"]?.let { bytes ->
            passWithData = parseJson(String(bytes), resources)
            // TODO fix ugly null issues
            if (passWithData != null) {
                passWithData!!.barcode?.let { barcode ->
                    // fill qr code
                    qr_code_detail.setImageBitmap(barcode.bitmap())
                    if (barcode.altText.isNotEmpty() && barcode.altText.isNotBlank()) {
                        detail_alt_text.text = barcode.altText
                    } else {
                        detail_alt_text.visibility = View.INVISIBLE
                    }
                }

                // fill fields
                listOf(
                    FieldType.primaryFields,
                    FieldType.secondaryFields,
                    FieldType.auxiliaryFields
                ).forEach {
                    val layout = LinearLayout(applicationContext)
                    passWithData!!.fields
                        .filter { field -> field.type == it }
                        .map { field ->
                            fieldLayout(
                                field,
                                passWithData!!.pass.labelColor,
                                passWithData!!.pass.foregroundColor
                            )
                        }.forEach { fieldView -> layout.addView(fieldView) }
                    detail_fields.addView(layout)
                }

                // customize colors
                detail_pass_card.setBackgroundColor(passWithData!!.pass.backgroundColor)
                detail_alt_text.setTextColor(passWithData!!.pass.foregroundColor)

                val dao = DatabaseSingleton.getDb(applicationContext).passDao()
                val passId = dao.insert(passWithData!!.pass)
                passWithData!!.barcode?.let {
                    it.fkPassId = passId
                    dao.insertAll(it)
                }

                passWithData!!.fields.forEach {
                    it.fkPassId = passId
                    dao.insertAll(it)
                }

                passWithData!!.resources.forEach {
                    it.fkPassId = passId
                    dao.insertAll(it)
                }
            }

        }

        zipContent.filter { it.key.startsWith("logo") }.maxBy { it.key }?.let {
            handleLogo(it.value)
            return
        }
    }

    private fun mapZipContentToResources(zipContent: Map<String, ByteArray>): List<Resource> {
        return zipContent.entries.map {
            Resource(
                name = it.key,
                type = if (it.key.endsWith("json")) ResourceType.JSON else ResourceType.PNG,
                data = it.value
            )
        }
    }

    private fun handleLogo(data: ByteArray) {
        val logo = BitmapFactory.decodeByteArray(
            data, 0, data.size
        )

        detail_logo.setImageBitmap(logo)
    }

    private fun readZip(input: InputStream): Map<String, ByteArray> {
        val zipInput = ZipInputStream(input)

        val zipContent: HashMap<String, ByteArray> = HashMap()

        while (true) {
            val entry = zipInput.nextEntry ?: break
            zipContent[entry.name] = zipInput.readBytes()
        }

        return zipContent
    }

    private fun fieldLayout(
        field: Field,
        labelColor: Int,
        valueColor: Int
    ): TableLayout {
        val fieldView = field.fieldView(applicationContext, labelColor, valueColor)
        val textView = (fieldView.getChildAt(1) as TableRow).getChildAt(0) as TextView
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, field.fontSize())
        return fieldView
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_detail, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_delete -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}
